package com.jhipster.padbowl.common.domain.error;

public enum ErrorStatus {
  BAD_REQUEST,
  UNAUTHORIZED,
  FORBIDDEN,
  NOT_FOUND,
  CONFLICT,
  INTERNAL_SERVER_ERROR,
}

package com.jhipster.padbowl.game.application;

import com.jhipster.padbowl.common.application.NotSecured;
import com.jhipster.padbowl.common.domain.Guttered;
import com.jhipster.padbowl.game.domain.Game;
import com.jhipster.padbowl.game.domain.GameEventsDispatcher;
import com.jhipster.padbowl.game.domain.GameId;
import com.jhipster.padbowl.game.domain.GamesRepository;
import java.util.Collection;
import java.util.Optional;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class GamesApplicationService {
  private final GamesRepository games;
  private final GameEventsDispatcher dispatcher;

  public GamesApplicationService(GamesRepository games, GameEventsDispatcher dispatcher) {
    this.games = games;
    this.dispatcher = dispatcher;
  }

  @NotSecured
  public Game create(String playerName) {
    Game game = new Game(playerName);

    games.save(game);

    return game;
  }

  @PreAuthorize("can('roll', #gameId)")
  public Game roll(GameId gameId, int roll) {
    Game game = games.get(gameId).orElseThrow(UnknownGameException::new);

    Collection<Guttered> events = game.roll(roll);
    dispatcher.dispatch(events);
    games.save(game);

    return game;
  }

  @NotSecured
  @Transactional(readOnly = true)
  public Optional<Game> get(GameId gameId) {
    return games.get(gameId);
  }
}

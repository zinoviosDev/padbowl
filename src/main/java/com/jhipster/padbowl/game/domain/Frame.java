package com.jhipster.padbowl.game.domain;

import java.util.Optional;

public class Frame {
  private final int firstRoll;
  private Optional<Integer> secondRoll;

  public Frame(int firstRoll) {
    assertFirstRoll(firstRoll);

    this.firstRoll = firstRoll;
    secondRoll = Optional.empty();
  }

  private void assertFirstRoll(int firstRoll) {
    if (firstRoll < 0 || firstRoll > 10) {
      throw new InvalidRollException();
    }
  }

  public int getFirstRoll() {
    return firstRoll;
  }

  public Frame secondRoll(int secondRoll) {
    assertSecondRoll(secondRoll);

    this.secondRoll = Optional.of(Integer.valueOf(secondRoll));

    return this;
  }

  private void assertSecondRoll(int secondRoll) {
    if (this.secondRoll.isPresent()) {
      throw new InvalidRollException();
    }

    if (secondRoll < 0 || firstRoll + secondRoll > 10) {
      throw new InvalidRollException();
    }
  }

  public Optional<Integer> getSecondRoll() {
    return secondRoll;
  }

  public boolean isTerminated() {
    return isStrike() || secondRoll.isPresent();
  }

  private boolean isStrike() {
    return firstRoll == 10;
  }
}

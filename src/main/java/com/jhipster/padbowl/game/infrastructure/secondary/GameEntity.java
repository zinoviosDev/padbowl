package com.jhipster.padbowl.game.infrastructure.secondary;

import com.jhipster.padbowl.common.infrastructure.Generated;
import com.jhipster.padbowl.game.domain.Frame;
import com.jhipster.padbowl.game.domain.Game;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;
import java.util.function.Function;
import java.util.function.IntFunction;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

@Entity
@Generated
@Table(name = "game")
class GameEntity {
  private static final Comparator<RollEntity> ROLL_COMPARATOR = Comparator.comparing(RollEntity::getRoll);

  @Id
  private UUID id;

  @Column(name = "player")
  private String player;

  @JoinColumn(name = "game")
  @OneToMany(cascade = CascadeType.ALL)
  private Collection<RollEntity> rolls;

  public static GameEntity from(Game game) {
    return new GameEntity().id(game.getId().get()).player(game.getPlayer().get()).rolls(buildRolls(game));
  }

  private static List<RollEntity> buildRolls(Game game) {
    List<Integer> pinsDown = game.getFrames().stream().flatMap(toPinsDown()).collect(Collectors.toList());

    return IntStream.range(0, pinsDown.size()).mapToObj(toRollEntity(game, pinsDown)).collect(Collectors.toList());
  }

  private static Function<Frame, Stream<Integer>> toPinsDown() {
    return frame ->
      frame.getSecondRoll().map(roll -> Stream.of(frame.getFirstRoll(), roll)).orElseGet(() -> Stream.of(frame.getFirstRoll()));
  }

  private static IntFunction<RollEntity> toRollEntity(Game game, List<Integer> pinsDown) {
    return roll -> new RollEntity().game(game.getId().get()).roll(roll).pinsDown(pinsDown.get(roll));
  }

  public Game toDomain() {
    Game game = new Game(getId(), getPlayer());

    rolls.stream().sorted(ROLL_COMPARATOR).map(RollEntity::getPinsDown).forEach(game::roll);

    return game;
  }

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public GameEntity id(UUID id) {
    this.id = id;

    return this;
  }

  public String getPlayer() {
    return player;
  }

  public void setPlayer(String player) {
    this.player = player;
  }

  public GameEntity player(String player) {
    this.player = player;

    return this;
  }

  public Collection<RollEntity> getRolls() {
    return rolls;
  }

  public void setRolls(Collection<RollEntity> rolls) {
    this.rolls = rolls;
  }

  public GameEntity rolls(Collection<RollEntity> rolls) {
    this.rolls = rolls;

    return this;
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder().append(id).hashCode();
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }

    if (obj == null || getClass() != obj.getClass()) {
      return false;
    }

    GameEntity other = (GameEntity) obj;
    return new EqualsBuilder().append(id, other.id).isEquals();
  }
}

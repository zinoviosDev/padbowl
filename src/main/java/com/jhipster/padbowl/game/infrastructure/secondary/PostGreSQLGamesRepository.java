package com.jhipster.padbowl.game.infrastructure.secondary;

import com.jhipster.padbowl.common.domain.error.Assert;
import com.jhipster.padbowl.game.domain.Game;
import com.jhipster.padbowl.game.domain.GameId;
import com.jhipster.padbowl.game.domain.GamesRepository;
import java.util.Optional;
import org.springframework.stereotype.Repository;

@Repository
class PostGreSQLGamesRepository implements GamesRepository {
  private final GamesSpringRepository games;

  public PostGreSQLGamesRepository(GamesSpringRepository games) {
    this.games = games;
  }

  @Override
  public void save(Game game) {
    Assert.notNull("game", game);

    games.save(GameEntity.from(game));
  }

  @Override
  public Optional<Game> get(GameId id) {
    Assert.notNull("id", id);

    return games.findById(id.get()).map(GameEntity::toDomain);
  }
}

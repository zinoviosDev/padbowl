package com.jhipster.padbowl.game.infrastructure.secondary;

import com.jhipster.padbowl.common.domain.Guttered;
import com.jhipster.padbowl.common.domain.error.Assert;
import com.jhipster.padbowl.game.domain.GameEventsDispatcher;
import java.util.Collection;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

@Component
class SpringGameEventsDispatcher implements GameEventsDispatcher {
  private final ApplicationEventPublisher publisher;

  public SpringGameEventsDispatcher(ApplicationEventPublisher publisher) {
    this.publisher = publisher;
  }

  @Override
  public void dispatch(Collection<Guttered> events) {
    Assert.notNull("events", events);

    events.forEach(publisher::publishEvent);
  }
}

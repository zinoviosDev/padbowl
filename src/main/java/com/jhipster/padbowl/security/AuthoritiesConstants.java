package com.jhipster.padbowl.security;

import com.jhipster.padbowl.common.infrastructure.Generated;

/**
 * Constants for Spring Security authorities.
 */
@Generated
public final class AuthoritiesConstants {
  public static final String ADMIN = "ROLE_ADMIN";

  public static final String USER = "ROLE_USER";

  public static final String ANONYMOUS = "ROLE_ANONYMOUS";

  public static final String PLAYER = "ROLE_PLAYER";

  private AuthoritiesConstants() {}
}

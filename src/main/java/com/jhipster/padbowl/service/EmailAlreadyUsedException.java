package com.jhipster.padbowl.service;

import com.jhipster.padbowl.common.domain.error.ErrorStatus;
import com.jhipster.padbowl.common.domain.error.PadBowlException;
import com.jhipster.padbowl.common.infrastructure.Generated;

@Generated
public class EmailAlreadyUsedException extends PadBowlException {
  private static final long serialVersionUID = 1L;

  public EmailAlreadyUsedException() {
    super(
      PadBowlException
        .builder(UserMessage.EMAIL_ALREADY_USED)
        .message("A user tried to create an account with an used email")
        .status(ErrorStatus.BAD_REQUEST)
    );
  }
}

package com.jhipster.padbowl.service;

import com.jhipster.padbowl.common.domain.error.ErrorStatus;
import com.jhipster.padbowl.common.domain.error.PadBowlException;
import com.jhipster.padbowl.common.infrastructure.Generated;

@Generated
public class LoginAlreadyUsedException extends PadBowlException {
  private static final long serialVersionUID = 1L;

  public LoginAlreadyUsedException() {
    super(
      PadBowlException
        .builder(UserMessage.LOGIN_ALREADY_USED)
        .status(ErrorStatus.BAD_REQUEST)
        .message("A user tried to create an account with an used login")
    );
  }
}

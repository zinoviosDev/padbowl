package com.jhipster.padbowl.service;

import com.jhipster.padbowl.common.domain.error.PadBowlMessage;

enum UserMessage implements PadBowlMessage {
  EMAIL_ALREADY_USED("user.e-mail-already-used"),
  LOGIN_ALREADY_USED("user.login-already-used"),
  INVALID_PASSWORD("user.invalid-password");

  private final String messageKey;

  private UserMessage(String messageKey) {
    this.messageKey = messageKey;
  }

  @Override
  public String getMessageKey() {
    return messageKey;
  }
}

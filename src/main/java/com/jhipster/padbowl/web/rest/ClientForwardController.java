package com.jhipster.padbowl.web.rest;

import com.jhipster.padbowl.common.infrastructure.Generated;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Generated
@Controller
public class ClientForwardController {

  /**
   * Forwards any unmapped paths (except those containing a period) to the client {@code index.html}.
   * @return forward to client {@code index.html}.
   */
  @GetMapping(value = "/**/{path:[^\\.]*}")
  public String forward() {
    return "forward:/";
  }
}

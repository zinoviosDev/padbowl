/**
 * View Models used by Spring MVC REST controllers.
 */
package com.jhipster.padbowl.web.rest.vm;

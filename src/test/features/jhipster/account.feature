Feature: Accounts management

  Scenario: Can't get authentication for not authenticated user
    Given I am not logged in
    When I get my account information
    Then I should not be authorized
    
  Scenario: Get account informations
    Given I am logged in as "admin"
    When I get my account information
    Then My login should be "admin"
    And My email should be "admin@localhost"
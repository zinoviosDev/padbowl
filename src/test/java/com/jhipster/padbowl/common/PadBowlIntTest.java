package com.jhipster.padbowl.common;

import com.jhipster.padbowl.PadBowlApp;
import io.github.jhipster.config.JHipsterConstants;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.transaction.Transactional;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.ActiveProfiles;

@Transactional
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@ActiveProfiles(JHipsterConstants.SPRING_PROFILE_TEST)
@SpringBootTest(classes = PadBowlApp.class, webEnvironment = WebEnvironment.RANDOM_PORT)
public @interface PadBowlIntTest {
}

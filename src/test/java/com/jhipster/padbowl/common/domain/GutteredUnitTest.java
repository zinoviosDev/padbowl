package com.jhipster.padbowl.common.domain;

import static com.jhipster.padbowl.game.domain.GamesFixture.*;
import static org.assertj.core.api.Assertions.*;

import com.jhipster.padbowl.common.domain.Guttered;
import com.jhipster.padbowl.common.domain.error.MissingMandatoryValueException;
import org.junit.jupiter.api.Test;

class GutteredUnitTest {

  @Test
  void shouldNotBuildWithoutPlayerName() {
    assertThatThrownBy(() -> new Guttered(null))
      .isExactlyInstanceOf(MissingMandatoryValueException.class)
      .hasMessageContaining("playerName");
  }

  @Test
  void shouldGetPlayerName() {
    assertThat(new Guttered(playerName()).getPlayerName()).isEqualTo(playerName());
  }
}

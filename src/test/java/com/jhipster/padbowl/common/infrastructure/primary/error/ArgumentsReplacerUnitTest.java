package com.jhipster.padbowl.common.infrastructure.primary.error;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Map;
import org.junit.jupiter.api.Test;

class ArgumentsReplacerUnitTest {

  @Test
  public void shouldNotReplaceArgumentsInNullMessage() {
    assertThat(ArgumentsReplacer.replaceArguments(null, Map.of("key", "value"))).isNull();
  }

  @Test
  public void shouldNotReplaceArgumentsWithoutArguments() {
    assertThat(ArgumentsReplacer.replaceArguments("Hey {{ user }}", null)).isEqualTo("Hey {{ user }}");
  }

  @Test
  public void shouldReplaceKnownArguments() {
    assertThat(ArgumentsReplacer.replaceArguments("Hey {{ user }}, how's {{ friend }} doing? Say {{user}}", Map.of("user", "Joe")))
      .isEqualTo("Hey Joe, how's {{ friend }} doing? Say Joe");
  }
}

package com.jhipster.padbowl.common.infrastructure.primary.error;

import static org.assertj.core.api.Assertions.*;

import com.jhipster.padbowl.common.infrastructure.primary.TestJson;
import org.junit.jupiter.api.Test;

class PadBowlFieldErrorUnitTest {

  @Test
  public void shouldGetFieldErrorInformation() {
    PadBowlFieldError fieldError = defaultFieldError();

    assertThat(fieldError.getFieldPath()).isEqualTo("path");
    assertThat(fieldError.getReason()).isEqualTo("reason");
    assertThat(fieldError.getMessage()).isEqualTo("message");
  }

  @Test
  public void shouldSerializeToJson() {
    assertThat(TestJson.writeAsString(defaultFieldError())).isEqualTo(defaultJson());
  }

  static PadBowlFieldError defaultFieldError() {
    return PadBowlFieldError.builder().fieldPath("path").reason("reason").message("message").build();
  }

  static String defaultJson() {
    return "{\"fieldPath\":\"path\",\"reason\":\"reason\",\"message\":\"message\"}";
  }
}

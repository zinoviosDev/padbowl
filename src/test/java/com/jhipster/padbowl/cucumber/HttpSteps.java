package com.jhipster.padbowl.cucumber;

import static org.assertj.core.api.Assertions.*;

import io.cucumber.java.en.Then;
import org.springframework.http.HttpStatus;

public class HttpSteps {

  @Then("I can't find document")
  public void shouldGetNotFoundResult() {
    assertThat(CucumberTestContext.getStatus()).isEqualTo(HttpStatus.NOT_FOUND);
  }

  @Then("I should not be authorized")
  public void shouldNotBeAuthorized() {
    assertThat(CucumberTestContext.getStatus()).isIn(HttpStatus.UNAUTHORIZED, HttpStatus.FORBIDDEN);
  }
}

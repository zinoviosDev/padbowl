package com.jhipster.padbowl.game.application;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;

import com.jhipster.padbowl.game.domain.GameId;
import com.jhipster.padbowl.game.domain.GamesFixture;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
class GameIdAccessCheckerUnitTest {
  @Mock
  private GamesApplicationService games;

  @InjectMocks
  private GameIdAccessChecker checker;

  @Test
  void shouldNotBeAuthorizedToDoUnknownAction() {
    assertThat(checker.can(authentication("Player"), "unknown", new GameId())).isFalse();
  }

  @Test
  void shouldNotBeAuthorizedToRollOnUnknownGame() {
    assertThat(checker.can(authentication("Player"), "roll", new GameId())).isFalse();
  }

  @Test
  void shouldNotBeAuthorizedToRollOnSomebodyElsesGame() {
    GameId gameId = new GameId();
    when(games.get(gameId)).thenReturn(Optional.of(GamesFixture.threeRolls()));

    assertThat(checker.can(authentication("notPlayer"), "roll", gameId)).isFalse();
  }

  @Test
  void shouldBeAuthorizedToRollOnOwnGame() {
    GameId gameId = new GameId();
    when(games.get(gameId)).thenReturn(Optional.of(GamesFixture.threeRolls()));

    assertThat(checker.can(authentication("Player"), "roll", gameId)).isTrue();
  }

  private UsernamePasswordAuthenticationToken authentication(String name) {
    return new UsernamePasswordAuthenticationToken(name, name, List.of(new SimpleGrantedAuthority("PLAYER")));
  }
}

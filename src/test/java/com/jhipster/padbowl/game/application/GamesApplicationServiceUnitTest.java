package com.jhipster.padbowl.game.application;

import static org.assertj.core.api.Assertions.*;

import com.jhipster.padbowl.game.domain.GameId;
import com.jhipster.padbowl.game.domain.GamesRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
class GamesApplicationServiceUnitTest {
  @Mock
  private GamesRepository repository;

  @InjectMocks
  private GamesApplicationService service;

  @Test
  void shouldNotRollOnUnknownGame() {
    assertThatThrownBy(() -> service.roll(new GameId(), 1)).isExactlyInstanceOf(UnknownGameException.class);
  }
}

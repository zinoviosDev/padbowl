package com.jhipster.padbowl.game.application;

import static org.assertj.core.api.Assertions.*;

import com.jhipster.padbowl.common.domain.error.ErrorStatus;
import com.jhipster.padbowl.game.domain.GameMessage;
import org.junit.jupiter.api.Test;

class UnknownGameExceptionUnitTest {

  @Test
  void shouldGetExceptionInformation() {
    UnknownGameException exception = new UnknownGameException();

    assertThat(exception.getPadBowlMessage()).isEqualTo(GameMessage.UNKNOWN_GAME);
    assertThat(exception.getMessage()).contains("unknown game");
    assertThat(exception.getStatus()).isEqualTo(ErrorStatus.NOT_FOUND);
  }
}

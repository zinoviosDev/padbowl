package com.jhipster.padbowl.game.domain;

import static org.assertj.core.api.Assertions.*;

import org.junit.jupiter.api.Test;

class FrameUnitTest {

  @Test
  void shouldNotAllowNegativeFirstRoll() {
    assertThatThrownBy(() -> new Frame(-1)).isExactlyInstanceOf(InvalidRollException.class);
  }

  @Test
  void shouldNotAllowFirstRollOverTen() {
    assertThatThrownBy(() -> new Frame(11)).isExactlyInstanceOf(InvalidRollException.class);
  }

  @Test
  void shouldGetOneRollFrameInformation() {
    Frame frame = new Frame(2);

    assertThat(frame.getFirstRoll()).isEqualTo(2);
    assertThat(frame.getSecondRoll()).isEmpty();
  }

  @Test
  void shouldNotAllowNegativeSecondRoll() {
    assertThatThrownBy(() -> new Frame(4).secondRoll(-1)).isExactlyInstanceOf(InvalidRollException.class);
  }

  @Test
  void shouldNotAllowSecondRollGoingOverTen() {
    assertThatThrownBy(() -> new Frame(4).secondRoll(7)).isExactlyInstanceOf(InvalidRollException.class);
  }

  @Test
  void shouldNotAllowMultipleSecondRolls() {
    assertThatThrownBy(() -> new Frame(4).secondRoll(1).secondRoll(1)).isExactlyInstanceOf(InvalidRollException.class);
  }

  @Test
  void shouldGetTwoRollsFrameInformation() {
    Frame frame = new Frame(4).secondRoll(2);

    assertThat(frame.getFirstRoll()).isEqualTo(4);
    assertThat(frame.getSecondRoll().get()).isEqualTo(2);
  }
}

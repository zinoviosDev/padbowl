package com.jhipster.padbowl.game.domain;

import static com.jhipster.padbowl.game.domain.GamesFixture.*;
import static org.assertj.core.api.Assertions.*;

import java.util.UUID;
import org.junit.jupiter.api.Test;

class GameIdUnitTest {

  @Test
  void shouldGetRandomUniqueIdFromDefaultConstructor() {
    GameId id = new GameId();

    assertThat(id.get()).isNotNull();
    assertThat(id.get()).isEqualTo(id.get());
  }

  @Test
  void shouldGetRandomUniqueIdFromConstructorWithoutId() {
    GameId id = new GameId(null);

    assertThat(id.get()).isNotNull();
    assertThat(id.get()).isEqualTo(id.get());
  }

  @Test
  void shouldGetInputId() {
    UUID id = UUID.randomUUID();
    GameId gameId = new GameId(id);

    assertThat(gameId.get()).isEqualTo(id);
  }

  @Test
  void shouldBeEqualToSelf() {
    GameId gameId = gameId();

    assertThat(gameId.equals(gameId)).isTrue();
  }

  @Test
  void shouldNotBeEqualToNull() {
    assertThat(gameId().equals(null)).isFalse();
  }

  @Test
  @SuppressWarnings("unlikely-arg-type")
  void shouldNotBeEqualToAnotherClass() {
    assertThat(gameId().equals("id")).isFalse();
  }

  @Test
  void shouldNotBeEqualToGameIdWithAnotherId() {
    assertThat(gameId().equals(new GameId())).isFalse();
  }

  @Test
  void shouldBeEqualToGameIdWithSameId() {
    assertThat(gameId().equals(gameId())).isTrue();
  }

  @Test
  void shouldHaveSameHashCodeWithSameId() {
    assertThat(gameId().hashCode()).isEqualTo(gameId().hashCode());
  }

  @Test
  void shouldHaveDifferentHashCodeWithDifferentId() {
    assertThat(gameId().hashCode()).isNotEqualTo(new GameId().hashCode());
  }
}

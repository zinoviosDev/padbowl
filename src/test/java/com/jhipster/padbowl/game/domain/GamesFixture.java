package com.jhipster.padbowl.game.domain;

import java.util.UUID;

public final class GamesFixture {

  private GamesFixture() {}

  public static PlayerName playerName() {
    return new PlayerName("Player");
  }

  public static Game threeRolls() {
    Game game = new Game(id(), "Player");

    game.roll(10);
    game.roll(6);
    game.roll(4);

    return game;
  }

  public static GameId gameId() {
    return new GameId(id());
  }

  public static UUID id() {
    return UUID.fromString("ff0ac298-3959-4969-a930-8b7aa36ad939");
  }
}

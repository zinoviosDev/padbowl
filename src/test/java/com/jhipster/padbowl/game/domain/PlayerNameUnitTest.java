package com.jhipster.padbowl.game.domain;

import static com.jhipster.padbowl.game.domain.GamesFixture.*;
import static org.assertj.core.api.Assertions.*;

import com.jhipster.padbowl.common.domain.error.MissingMandatoryValueException;
import com.jhipster.padbowl.common.domain.error.StringTooLongException;
import org.junit.jupiter.api.Test;

class PlayerNameUnitTest {

  @Test
  void shouldNotBuildWithoutPlayerName() {
    assertThatThrownBy(() -> new PlayerName(null))
      .isExactlyInstanceOf(MissingMandatoryValueException.class)
      .hasMessageContaining("playerName");
  }

  @Test
  void shouldNotBuildWithBlankPlayerName() {
    assertThatThrownBy(() -> new PlayerName(" "))
      .isExactlyInstanceOf(MissingMandatoryValueException.class)
      .hasMessageContaining("playerName");
  }

  @Test
  void shouldNotBuildWithFiftyOneCharactersPlayerName() {
    assertThatThrownBy(() -> new PlayerName("A".repeat(51)))
      .isExactlyInstanceOf(StringTooLongException.class)
      .hasMessageContaining("playerName");
  }

  @Test
  void shouldGetName() {
    assertThat(playerName().get()).isEqualTo("Player");
  }

  @Test
  void shouldBeEqualToSelf() {
    PlayerName playerName = playerName();

    assertThat(playerName.equals(playerName)).isTrue();
  }

  @Test
  void shouldNotBeEqualToNull() {
    assertThat(playerName().equals(null)).isFalse();
  }

  @Test
  @SuppressWarnings("unlikely-arg-type")
  void shouldNotBeEqualToAnotherClass() {
    assertThat(playerName().equals("Player")).isFalse();
  }

  @Test
  void shouldNotBeEqualToPlayerNameWithAnotherName() {
    assertThat(playerName().equals(new PlayerName("another"))).isFalse();
  }

  @Test
  void shouldBeEqualToPlayerNameWithSameName() {
    assertThat(playerName().equals(playerName())).isTrue();
  }

  @Test
  void shouldHaveSameHashCodeWithSameName() {
    assertThat(playerName().hashCode()).isEqualTo(playerName().hashCode());
  }

  @Test
  void shouldHaveDifferentHashCodeWithDifferentName() {
    assertThat(playerName().hashCode()).isNotEqualTo(new PlayerName("different").hashCode());
  }
}

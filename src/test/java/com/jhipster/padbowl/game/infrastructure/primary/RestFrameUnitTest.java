package com.jhipster.padbowl.game.infrastructure.primary;

import static org.assertj.core.api.Assertions.*;

import com.jhipster.padbowl.common.infrastructure.primary.TestJson;
import com.jhipster.padbowl.game.domain.Frame;
import org.junit.jupiter.api.Test;

class RestFrameUnitTest {

  @Test
  void shouldSerializeOneRollFrame() {
    assertThat(TestJson.writeAsString(RestFrame.from(new Frame(2)))).isEqualTo("{\"firstRoll\":2}");
  }

  @Test
  void shouldSerializeTwoRollsFrame() {
    assertThat(TestJson.writeAsString(RestFrame.from(new Frame(2).secondRoll(4)))).isEqualTo("{\"firstRoll\":2,\"secondRoll\":4}");
  }
}

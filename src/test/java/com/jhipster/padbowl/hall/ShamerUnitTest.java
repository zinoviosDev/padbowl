package com.jhipster.padbowl.hall;

import ch.qos.logback.classic.Level;
import com.jhipster.padbowl.common.LogSpy;
import com.jhipster.padbowl.common.domain.Guttered;
import com.jhipster.padbowl.game.domain.GamesFixture;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.PosixFilePermission;
import java.nio.file.attribute.PosixFilePermissions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.context.PayloadApplicationEvent;

@ExtendWith(LogSpy.class)
class ShamerUnitTest {
  private final LogSpy logs;

  public ShamerUnitTest(LogSpy logs) {
    this.logs = logs;
  }

  @Test
  void shouldHandleShamingError() {
    Path directory = createReadOnlyDirectory();
    Shamer dummyShamer = new Shamer(directory + "/test");

    dummyShamer.onApplicationEvent(guttered());

    logs.assertLogged(Level.ERROR, "target/not-writable");
  }

  private Path createReadOnlyDirectory() {
    try {
      Path directory = Files.createDirectories(Paths.get("target/not-writable"));
      Files.setPosixFilePermissions(directory, PosixFilePermissions.fromString("r--------"));
      return directory;
    } catch (IOException e) {
      throw new AssertionError(e.getMessage());
    }
  }

  private PayloadApplicationEvent<Guttered> guttered() {
    return new PayloadApplicationEvent<>(this, new Guttered(GamesFixture.playerName()));
  }
}

package com.jhipster.padbowl.kipe;

import static org.assertj.core.api.Assertions.*;

import com.jhipster.padbowl.common.domain.Username;
import com.jhipster.padbowl.common.infrastructure.primary.error.NotAuthenticatedUserException;
import com.jhipster.padbowl.security.AuthoritiesConstants;
import org.junit.jupiter.api.Test;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.Authentication;

class PadBowlAuthorizationsUnitTest {

  @Test
  void shouldNotGetUsernameWithoutAuthentication() {
    assertThatThrownBy(() -> PadBowlAuthorizations.getUsername(null)).isExactlyInstanceOf(NotAuthenticatedUserException.class);
  }

  @Test
  void shouldGetUsernameFromAuthentication() {
    assertThat(PadBowlAuthorizations.getUsername(player())).isEqualTo(new Username("user"));
  }

  @Test
  void shouldNotBeAllAuthorizedWhenCheckingWithoutAuthentication() {
    assertThat(PadBowlAuthorizations.allAuthorized(null, "action", Resource.GAMES)).isFalse();
  }

  @Test
  void shouldNotBeAllAuthorizedWhenCheckingWithoutAction() {
    assertThat(PadBowlAuthorizations.allAuthorized(player(), null, Resource.GAMES)).isFalse();
  }

  @Test
  void shouldNotBeAllAuthorizedWhenCheckingWithBlankAction() {
    assertThat(PadBowlAuthorizations.allAuthorized(player(), " ", Resource.GAMES)).isFalse();
  }

  @Test
  void shouldNotBeAllAuthorizedWhenCheckingWithoutResource() {
    assertThat(PadBowlAuthorizations.allAuthorized(player(), "action", null)).isFalse();
  }

  @Test
  void shouldNotBeAllAuthorizedWhenCheckingForUnauthorizedAction() {
    assertThat(PadBowlAuthorizations.allAuthorized(player(), "save", Resource.GAMES)).isFalse();
  }

  @Test
  void shouldNotBeAllAuthorizedWhenCheckingForSpecificAuthorizedAction() {
    assertThat(PadBowlAuthorizations.allAuthorized(player(), "order", Resource.PLAYERS)).isFalse();
  }

  @Test
  void shouldNotBeAllAuthorizedWhenCheckingForAllUnauthorizedNoRoleAction() {
    assertThat(PadBowlAuthorizations.allAuthorized(admin(), "read", Resource.PLAYERS)).isFalse();
  }

  @Test
  void shouldNotBeAllAuthorizedWhenCheckingForUnknownAction() {
    assertThat(PadBowlAuthorizations.allAuthorized(admin(), "dummy", Resource.GAMES)).isFalse();
  }

  @Test
  void shouldBeAllAuthorizedWhenCheckingForAllAuthorizedAction() {
    assertThat(PadBowlAuthorizations.allAuthorized(admin(), "create", Resource.GAMES)).isTrue();
  }

  @Test
  void shouldNotBeSpecificAuthorizedWhenCheckingWithoutAuthentication() {
    assertThat(PadBowlAuthorizations.specificAuthorized(null, "action", Resource.GAMES)).isFalse();
  }

  @Test
  void shouldNotBeSpecificAuthorizedWhenCheckingWithoutAction() {
    assertThat(PadBowlAuthorizations.specificAuthorized(player(), null, Resource.GAMES)).isFalse();
  }

  @Test
  void shouldNotBeSpecificAuthorizedWhenCheckingWithBlankAction() {
    assertThat(PadBowlAuthorizations.specificAuthorized(player(), " ", Resource.GAMES)).isFalse();
  }

  @Test
  void shouldNotBeSpecificAuthorizedWhenCheckingWithoutResource() {
    assertThat(PadBowlAuthorizations.specificAuthorized(player(), "action", null)).isFalse();
  }

  @Test
  void shouldNotBeSpecificAuthorizedWhenCheckingForUnauthorizedAction() {
    assertThat(PadBowlAuthorizations.specificAuthorized(player(), "save", Resource.GAMES)).isFalse();
  }

  @Test
  void shouldNotBeSpecificAuthorizedWhenCheckingForUnknownAction() {
    assertThat(PadBowlAuthorizations.specificAuthorized(admin(), "dummy", Resource.GAMES)).isFalse();
  }

  @Test
  void shouldBeSpecificAuthorizedWhenCheckingForAllAuthorizedNoRoleAction() {
    assertThat(PadBowlAuthorizations.specificAuthorized(admin(), "create", Resource.GAMES)).isTrue();
  }

  @Test
  void shouldBeSpecificAuthorizedWhenCheckingForSpecificAuthorizedNoRoleAction() {
    assertThat(PadBowlAuthorizations.specificAuthorized(admin(), "roll", Resource.GAMES)).isTrue();
  }

  private Authentication player() {
    return new TestingAuthenticationToken("user", null, AuthoritiesConstants.PLAYER);
  }

  private Authentication admin() {
    return new TestingAuthenticationToken("gerard", null, AuthoritiesConstants.ADMIN);
  }
}
